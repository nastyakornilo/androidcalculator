package com.kornilo.admin.calculator;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements com.kornilo.admin.calculator.Communicator, com.kornilo.admin.calculator.TextShowingActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TextView output;
    private SimpleCalculator tab0_simple_calc;
//
//    private String value = "";
//
//    public String getValue(){
//        return value;
//    }
//
//    public void setValue(String newValue){
//        value = newValue;
//    }
//
//    public LinkedList<String> operators = new LinkedList<String>();
//


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mSectionsPagerAdapter.startUpdate(mViewPager);
        //tab0_simple_calc = (SimpleCalculator) mSectionsPagerAdapter.instantiateItem(mViewPager, 0);
        //tab1 = (Fragment1) mSectionsPagerAdapter.instantiateItem(viewPager, 1);
        mSectionsPagerAdapter.finishUpdate(mViewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


//        if (savedInstanceState != null) {
//            //Restore the fragment's instance
//            mContent = getSupportFragmentManager().getFragment(savedInstanceState, "myFragmentName");
//
//        }
        output = (TextView) findViewById(com.kornilo.admin.calculator.R.id.textViewAns);
        findViewById(R.id.btnClear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                output.setText("");

                //set calc_data in fragments
//                calc_data.setLastIsNumber(false);
//                calc_data.setStateError(false);
//                calc_data.setIsDotLast(false);
//                //not good idea ((((
                FragmentManager manager = getSupportFragmentManager();

                List<Fragment> fragments = manager.getFragments();
                boolean on_border_tab = fragments.size() == 3;

                int item_index = mViewPager.getCurrentItem();
                boolean on_programmer_calc = item_index == 2;
                Fragment f;
                if (on_programmer_calc){
                    f = manager.getFragments().get(1);
                    ((ProgrammerCalcFragment)f).reset_calc_data();
                }
                else {
                    f = manager.getFragments().get(0);
                    ((SimpleCalculator)f).reset_calc_data();
                    //SimpleCalculator frag = (SimpleCalculator) manager.getFragments().get(0);
                }


            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

//      SimpleCalculator simpleCalculatorFragment;
//    public void registerKey(View view){
//
//        simpleCalculatorFragment.registerKey(view);
//    }

    @Override public void respond(CalculatorData data) {
        FragmentManager manager = getSupportFragmentManager();
        for (Fragment frag: manager.getFragments()) {
            if (frag instanceof SimpleCalculator){
                ((SimpleCalculator) frag).change(data);
                break;
            }
        }
    }

    public void show(String s)
    {
        output.setText(s);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SimpleCalculator();
                case 1:
                    return new ExtendedCalculatorFragment();

                case 2:
                    return new ProgrammerCalcFragment();



            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.simple_calc_tab_name);
                case 1:
                    return getResources().getString(R.string.exteneded_calc_tab_name);
                case 2:
                    return getResources().getString(R.string.programmer_calc_tab_name);
            }
            return null;
        }
    }
}
