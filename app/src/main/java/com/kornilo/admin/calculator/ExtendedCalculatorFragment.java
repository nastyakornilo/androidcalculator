package com.kornilo.admin.calculator;

import android.app.Activity;
import android.content.Context;
//import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

//import com.kornilo.admin.myapplication.databinding.SimpleCalculatorBinding;

import java.util.LinkedList;

import static android.R.attr.maxLength;


public class ExtendedCalculatorFragment extends Fragment {

//    public final static String tag = "extended calculator";
    private int[] needBrackets = {R.id.btnSin, R.id.btnCos, R.id.btnSinh, R.id.btnCosh, R.id.btnLog2, R.id.btnLog10, R.id.btnSqrt, R.id.btnPow,};
    private int[] rationalNumbers = {R.id.btnPi, R.id.btnExp};
    private int[] Brackets = {R.id.btnLeftBracket, R.id.btnRightBracket};

    //private TextView txtScreen;
//    // Represent whether the lastly pressed key is numeric or not
//    private boolean lastNumeric;
//    // Represent that current state is in error or not
//    private boolean stateError;
//    // If true, do not allow to add another DOT
//    private boolean lastDot;
    private CalculatorData calc_data;

    private Communicator comm;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Communicator){
            comm = (Communicator)context;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.extended_calculator, container, false);

        setNeedBracketsOperatorOnClickListener(rootView);
        setNotNeedBracketsOperatorOnClickListener(rootView);
        setNumericOnClickListener(rootView);

        return rootView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
////        Bundle savedData = getIntent().getExtras();
//        if (savedInstanceState != null){
//            calc_data = savedInstanceState.getParcelable("calc_data");
//        }
//        else
            calc_data=new CalculatorData("",false,false,false);
//            txtScreen.setText(savedData.getString("expression"));
//            lastNumeric = savedData.getBoolean("lastNumber");
//            stateError = savedData.getBoolean("stateError");
//            lastDot = savedData.getBoolean("Dot");
//        }
    }

//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        outState.putParcelable("calc_data", calc_data);
//        super.onSaveInstanceState(outState);
//
//    }
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        if (savedInstanceState != null) {
//            calc_data = savedInstanceState.getParcelable("calc_data");
//        }
//    }

//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        txtScreen.setText(savedInstanceState.getString("expression"));
//        lastNumeric = savedInstanceState.getBoolean("lastNumber");
//        stateError = savedInstanceState.getBoolean("stateError");
//        lastDot = savedInstanceState.getBoolean("Dot");
//    }
    @Override
    public  void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser)
            comm.respond(calc_data);
    }
    private void setNumericOnClickListener(View view) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button = (Button) v;
                if (calc_data.getStateError()) {
                    calc_data.setExpression(button.getText().toString());
                    calc_data.setStateError(false);
                } else {
                    calc_data.setExpression(button.getText().toString());
                }
                calc_data.setLastIsNumber(true);
                comm.respond(calc_data);
                reset_calc_data();
            }
        };
        // Assign the listener to all the numeric buttons
        for (int id : rationalNumbers) {
            view.findViewById(id).setOnClickListener(listener);
        }
    }

    private void setNeedBracketsOperatorOnClickListener(View view) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!calc_data.getStateError()) {
                    Button button = (Button) v;
                    calc_data.setExpression(button.getText()+"(");
                    calc_data.setLastIsNumber(false);
                    calc_data.setIsDotLast(false);
                }
                comm.respond(calc_data);
                reset_calc_data();
            }
        };
        for (int id : needBrackets) {
            view.findViewById(id).setOnClickListener(listener);
        }
    }

    private void setNotNeedBracketsOperatorOnClickListener(View view) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button = (Button) v;
                if (!calc_data.getStateError()) {
                    calc_data.setExpression( button.getText().toString());
                    calc_data.setIsDotLast(false);
                }
                else{
                    calc_data.setExpression(button.getText().toString());
                    calc_data.setStateError(false);
                }
                comm.respond(calc_data);
                reset_calc_data();
            }
        };
        for (int id : Brackets) {
            view.findViewById(id).setOnClickListener(listener);
        }

    }

//    public static final String COS = "cos";
//    public static final String SIN = "sin";
//    public static final String TG = "tg";
//    public static final String CTG = "ctg";
//
//    private String value = "";
//    public String getValue(){
//        return value;
//    }
//    public LinkedList<String> operators = new LinkedList<String>();
//
//    private Communicator comm;
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//
//        if (context instanceof Communicator){
//            comm = (Communicator)context;
//        }
//
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View rootView = inflater.inflate(R.layout.extended_calculator, container, false);
//        return rootView;
//    }
//
//    public ExtendedCalculatorFragment() {
//    }
///*
//    @Override
//    public void onStart() {
//        value = ((MainActivity) getActivity()).getValue();
//        operators = ((MainActivity) getActivity()).operators;
//        super.onStart();
//    }
//
//    @Override
//    public void onPause() {
//        ((MainActivity) getActivity()).setValue(value);
//        ((MainActivity) getActivity()).operators = operators;
//        super.onPause();
//    }
//*/
//    public void registerKey(View view)
//    {
//        int viewId = view.getId();
//        if (!(getValue().length() == maxLength && viewId!=com.kornilo.admin.myapplication.R.id.buttonDel))
//            switch(viewId)
//            {
//                case R.id.buttonCos:
//                    safelyPlaceOperator(COS);
//                    break;
//                case R.id.buttonSin:
//                    safelyPlaceOperator(SIN);
//                    break;
//                case R.id.buttonTg:
//                    safelyPlaceOperator(TG);
//                    break;
//                case R.id.buttonCtg:
//                    safelyPlaceOperator(CTG);
//                    break;
//            }
//
//        comm.respond(calc_data);
//            //((MainActivity) getActivity()).display(value);
//    }
//    private void safelyPlaceOperator(String op)   ////!!!!!!!!!!!!!!!!!!!!
//    {
////        if (endsWithUnaryOperator())  // Avoid double operators by replacing old operator.
////            deleteFromLeft();
////        value += op;
////        operators.add(op);
//        // else: Operator by itself is an illegal operation, do not place operator.
//    }
//
//    private boolean endsWithUnaryOperator(){
//        return (value.endsWith(COS) || value.endsWith(SIN) || value.endsWith(TG) || value.endsWith(CTG));
//    }
//
////    public void deleteFromLeft()
////    {
////        if (value.length() > 0)
////        {
////            //if (endsWithUnaryOperator())
////            value = value.substring(0, value.length()-operators.getLast().length());
////            operators.removeLast();
////        }
////    }
//
//    private int findLastOperator()
//    {
//        int sin_idx = value.lastIndexOf(SIN);
//        int cos_idx = value.lastIndexOf(COS);
//        int tg_idx = value.lastIndexOf(TG);
//        int ctg_idx = value.lastIndexOf(CTG);
//        return Math.max(sin_idx, Math.max(cos_idx, Math.max(tg_idx, ctg_idx)));
//    }
//
//
    public void reset_calc_data(){
        if (!(calc_data == null)){
            calc_data.setExpression("");
            calc_data.setLastIsNumber(false);
            calc_data.setStateError(false);
            calc_data.setIsDotLast(false);
        }


}

}
