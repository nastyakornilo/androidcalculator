package com.kornilo.admin.calculator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;

/**
 * Created by Admin on 25.10.2017.
 */

public class ProgrammerCalcFragment extends Fragment {
    //public static String DTAG = "net.lawleagle";
    public static int MINIMUM_BASE = 2;
    public static int MAXIMUM_BASE = 16;

    private Button binButton;
    //private Button decButton;
    private Button hexButton;
    private NumberPicker numberPicker;
    private int numerationBase;

    private Button[] numericalButtons;
    private Button[] letterButtons;
    //private Button deleteButton;
    private Button andButton;
    private Button orButton;
    private Button xorButton;
    
    private CalculatorData calc_data;
    //private Button divideButton;
    private Button equalsButton;
    //private Button openParanthesis;
    //private Button closeParanthesis;

//    private TextView screen;
//    private TextView output;

    public ProgrammerCalcFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        calc_data=new CalculatorData("",false,false,false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.programmer_calc, container, false);

//        screen = (TextView) rootView.findViewById(R.id.screen);
//        output = (TextView) rootView.findViewById(R.id.output);

        binButton = (Button) rootView.findViewById(R.id.binButton);
        //decButton = (Button) rootView.findViewById(R.id.decButton);
        hexButton = (Button) rootView.findViewById(R.id.hexButton);
        andButton = (Button)rootView.findViewById(R.id.andButton);
        orButton = (Button)rootView.findViewById(R.id.orButton);
        xorButton = (Button)rootView.findViewById(R.id.xorButton);
        
        numberPicker = (NumberPicker) rootView.findViewById(R.id.numberPicker);
        numberPicker.setMaxValue(MAXIMUM_BASE);
        numberPicker.setMinValue(MINIMUM_BASE);
        numberPicker.setWrapSelectorWheel(true);

        numericalButtons = new Button[]{
                (Button) rootView.findViewById(R.id.button0),
                (Button) rootView.findViewById(R.id.button1),
                (Button) rootView.findViewById(R.id.button2),
                (Button) rootView.findViewById(R.id.button3),
                (Button) rootView.findViewById(R.id.button4),
                (Button) rootView.findViewById(R.id.button5),
                (Button) rootView.findViewById(R.id.button6),
                (Button) rootView.findViewById(R.id.button7),
                (Button) rootView.findViewById(R.id.button8),
                (Button) rootView.findViewById(R.id.button9)
        };
        letterButtons = new Button[]{
                (Button) rootView.findViewById(R.id.buttona),
                (Button) rootView.findViewById(R.id.buttonb),
                (Button) rootView.findViewById(R.id.buttonc),
                (Button) rootView.findViewById(R.id.buttond),
                (Button) rootView.findViewById(R.id.buttone),
                (Button) rootView.findViewById(R.id.buttonf)
        };
        //deleteButton = (Button) rootView.findViewById(R.id.deleteButton);
        //andButton = (Button) rootView.findViewById(R.id.andButton);
        //orButton = (Button) rootView.findViewById(R.id.orButton);
        //xorButton = (Button) findViewById(R.id.xorButton);
        //divideButton = (Button) findViewById(R.id.divideButton);
        equalsButton = (Button) rootView.findViewById(R.id.equalsButton);
        //openParanthesis = (Button) findViewById(R.id.openParanthesis);
        //closeParanthesis = (Button) findViewById(R.id.closeParanthesis);


        binButton.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BinButtonClicked();
                    }
                }
        );
//        decButton.setOnClickListener(
//                new Button.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        DecButtonClicked();
//                    }
//                }
//        );
        hexButton.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HexButtonClicked();
                    }
                }
        );
        numberPicker.setOnValueChangedListener(
                new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        SetNumerationBase(newVal);
                        ((TextShowingActivity) getActivity()).show(calc_data.getExpression());
                    }
                }
        );

        for (int i = 0; i <= 9; i++) {
            numericalButtons[i].setTag(i);
            numericalButtons[i].setOnClickListener(
                    new Button.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            NumericalButtonClicked((int) v.getTag());
                        }
                    });
        }
        for (int i = 0; i < 6; i++) {
            letterButtons[i].setTag(i);
            letterButtons[i].setOnClickListener(
                    new Button.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                           LetterButtonClicked((int) v.getTag());
                        }
                    }
            );
        }
//        deleteButton.setOnClickListener(
//                new Button.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        DeleteButtonClicked();
//                    }
//                });
//        
        andButton.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AndButtonClicked();
                    }
                });
        orButton.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        OrButtonClicked();
                    }
                });
        xorButton.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        XorButtonClicked();
                    }
                });
//        divideButton.setOnClickListener(
//                new Button.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        DivideButtonClicked();
//                    }
//                });
        equalsButton.setOnClickListener(
                new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UpdateOutput();
                    }
                }
        );
//        openParanthesis.setOnClickListener(
//                new Button.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        OpenParanthesisButtonClicked();
//                    }
//                }
//        );
//        closeParanthesis.setOnClickListener(
//                new Button.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        CloseParanthesisButtonClicked();
//                    }
//                }
//        );

        SetNumerationBase(10);
        return rootView;
    }

    private void ButtonStateInInterval(int start, int stop, boolean state) {
        for (int i = start; i <= Math.min(9, stop); i++) {
            numericalButtons[i].setEnabled(state);
        }
        for (int i = Math.max(start, 10); i < 16; i++) {
            letterButtons[i - 10].setEnabled(state);
        }
    }
    private void SetNumerationBase(int base) {
        int previousBase = numerationBase;
        numerationBase = base;

        ButtonStateInInterval(0, base - 1, true);
        ButtonStateInInterval(base, 15, false);
        numberPicker.setValue(base);

        calc_data.setExpression(BaseConversions.ConvertInt(calc_data.getExpression(), previousBase, numerationBase));

    }

    @Override
    public  void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            if (getActivity() != null) {
                if (calc_data != null)
                    ((TextShowingActivity) getActivity()).show(calc_data.getExpression());
                else
                    ((TextShowingActivity) getActivity()).show("");
            }
        }


    }

    private void BinButtonClicked() {
        SetNumerationBase(2);
        ((TextShowingActivity) getActivity()).show(calc_data.getExpression());
    }
//    private void DecButtonClicked() {
//        SetNumerationBase(10);
//        ((TextShowingActivity) getActivity()).show(calc_data.getExpression());
//    }
    private void HexButtonClicked() {
        SetNumerationBase(16);
        ((TextShowingActivity) getActivity()).show(calc_data.getExpression());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("calc_data", calc_data);
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            calc_data = savedInstanceState.getParcelable("calc_data");
            ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
        }


    }
    private void NumericalButtonClicked(int nr) {
        calc_data.setExpression(calc_data.getExpression() + Integer.toString(nr));
        ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
        calc_data.setLastIsNumber(true);
    }
    private void LetterButtonClicked(int nr) {
     //   screen.setText(screen.getText().toString() + (char)('a' + nr));
        calc_data.setExpression(calc_data.getExpression()+ (char)('a' + nr));
        ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
        calc_data.setLastIsNumber(true);
        //UpdateOutput();
    }
    private void DeleteLast() {
        int len = calc_data.getExpression().length();
        if (len  > 0) {
            calc_data.setExpression(calc_data.getExpression().substring(0, len - 1));
        }
    }

    private void AndButtonClicked() {
        if (!calc_data.getLastIsNumber()) {
            DeleteLast();
        }
        calc_data.setExpression(calc_data.getExpression() + '&');
        //screen.setText(screen.getText().toString() + '&');
        ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
        calc_data.setLastIsNumber(false);

    }
    private void OrButtonClicked() {
        calc_data.setExpression(calc_data.getExpression() + '|');
        //screen.setText(screen.getText().toString() + "|");
        ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
    }
    private void XorButtonClicked() {
        calc_data.setExpression(calc_data.getExpression() + '^');
        //screen.setText(screen.getText().toString() + '^');
        ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
    }
//    private void DivideButtonClicked() {
//        screen.setText(screen.getText().toString() + '/');
//    }
//    private void OpenParanthesisButtonClicked() {
//        screen.setText(screen.getText().toString() + '(');
//    }
//    private void CloseParanthesisButtonClicked() {
//        screen.setText(screen.getText().toString() + ')');
//    }

    public void reset_calc_data(){
        if (!(calc_data == null)){
            calc_data.setExpression("");
            calc_data.setLastIsNumber(false);
            calc_data.setStateError(false);
            calc_data.setIsDotLast(false);
            ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
        }

    }

    private void UpdateOutput() {
        try {
            ProgrammerCalculator ecuation = new ProgrammerCalculator(calc_data.getExpression(), numerationBase);
            calc_data.setExpression(ecuation.Result());
        } catch (Exception e) {
                calc_data.setExpression(getResources().getString(R.string.error));
                calc_data.setStateError(true);
                calc_data.setLastIsNumber(false);
        }
        finally {
            ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
        }
    }


//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        //getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
}
