package com.kornilo.admin.calculator;
import android.os.Parcel;
import android.os.Parcelable;

class CalculatorData implements Parcelable{
    private String expression;
    private boolean lastIsNumber;
    private boolean stateError;
    private boolean isDotLast;

    public CalculatorData(String expression, boolean lastIsNumber, boolean stateError, boolean isDotLast){
        this.expression=expression;
        this.lastIsNumber=lastIsNumber;
        this.stateError = stateError;
        this.isDotLast = isDotLast;
    }

    public String getExpression() {return expression;};
    public boolean getLastIsNumber () {return  lastIsNumber;};
    public boolean getStateError() {return  stateError;};
    public boolean getIsDotLast() {return  isDotLast;};

    public void setExpression(String expression) {this.expression=expression;};
    public void setLastIsNumber (boolean lastIsNumber) {this.lastIsNumber=lastIsNumber;};
    public void setStateError(boolean  stateError) {this.stateError = stateError;};
    public void setIsDotLast(boolean isDotLast) {this.isDotLast = isDotLast;};



    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(expression);
        out.writeByte((byte) (lastIsNumber ? 1 : 0));
        out.writeByte((byte) (stateError ? 1 : 0));
        out.writeByte((byte) (isDotLast ? 1 : 0));
    }

    public static final Parcelable.Creator<CalculatorData> CREATOR
            = new Parcelable.Creator<CalculatorData>() {
        public CalculatorData createFromParcel(Parcel in) {
            return new CalculatorData(in);
        }

        public CalculatorData[] newArray(int size) {
            return new CalculatorData[size];
        }
    };

    /** recreate object from parcel */
    private CalculatorData(Parcel in) {
        expression = in.readString();
        lastIsNumber = in.readByte() != 0;;
        stateError = in.readByte() != 0;;
        isDotLast = in.readByte() != 0;
    }

    public CalculatorData getDeepCopy(){
        return new CalculatorData(expression, lastIsNumber,stateError, isDotLast);
    }
}

public interface Communicator{
    public void respond(CalculatorData data);
}
