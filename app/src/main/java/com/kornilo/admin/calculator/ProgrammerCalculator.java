package com.kornilo.admin.calculator;

public class ProgrammerCalculator {
    private String ecuation;
    private int ecindex;
    private int numerationBase;

    public ProgrammerCalculator(String ecuation, int base) {
        this.ecuation = ecuation + "T";;
        this.numerationBase = base;
        this.ecindex = 0;
    }
    public ProgrammerCalculator(String ecuation) {
        this(ecuation, 10);
    }

    public String Result() {
        while (ecuation.charAt(ecuation.length() - 1) == '&' ||
                ecuation.charAt(ecuation.length() - 1) == '^' ||
                ecuation.charAt(ecuation.length() - 1) == '|') {
            ecuation = ecuation.substring(0, ecuation.length() - 1);
        }
        int result = EvalEcuation();
        return BaseConversions.ConvertInt(Integer.toString(result), 10, numerationBase);
    }

    private int EvalEcuation() {
        int nr = EvalTermen();

        while (ecuation.charAt(ecindex) == '|' || ecuation.charAt(ecindex) == '^') {
            switch (ecuation.charAt(ecindex)) {
                case '^': {
                    ecindex++;
                    nr = nr ^ EvalTermen();
                } break;

                case '|': {
                    ecindex++;
                    nr = nr | EvalTermen();
                } break;
            }
        }

        return nr;
    }
    private int EvalTermen() {
        //int nr = EvalFactor();
        int nr = ParseNumber();
        while (ecuation.charAt(ecindex) == '&') {
            ecindex++;
            //nr = nr & EvalFactor();
            nr = nr & ParseNumber();


        }

        return nr;
    }
    private int ParseNumber() {
        int nr = 0;

        while (IsNumber(ecuation.charAt(ecindex))) {
            nr = nr * numerationBase + CharDigitToInteger(ecuation.charAt(ecindex));
            ecindex++;
        }

        return nr;
    }
    private boolean IsNumber(char c) {
        if (('0' <= c && c <= '9') || ('a' <= c && c <= 'f')) {
            return true;
        }
        return false;
    }
    private int CharDigitToInteger(char c) {
        if ('a' <= c && c <= 'f') {
            return c - 'a' + 10;
        }
        return c - '0';
    }
}
//    private int EvalFactor() {
//        int nr;
//
//        if (ecuation.charAt(ecindex) == '(') {
//            ecindex++;
//            nr = EvalEcuation();
//            ecindex++;
//        } else {
//            nr = ParseNumber();
//        }
//
//        return nr;
//    }
