package com.kornilo.admin.calculator;

import android.content.Intent;
//import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

//import com.kornilo.admin.myapplication.databinding.SimpleCalculatorBinding;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.text.DecimalFormat;
import java.util.EmptyStackException;
import java.util.LinkedList;

import static android.app.Activity.RESULT_OK;


public class SimpleCalculator extends Fragment{

    // IDs of all the numeric buttons
    //public final static String TAG = "simple calculator";
    private int[] numericButtons = {R.id.btnZero, R.id.btnOne, R.id.btnTwo, R.id.btnThree, R.id.btnFour, R.id.btnFive, R.id.btnSix, R.id.btnSeven, R.id.btnEight, R.id.btnNine};
    // IDs of all the operator buttons
    private int[] operatorButtons = {R.id.btnAdd, R.id.btnSub, R.id.btnMul, R.id.btnDiv};
    // TextView used to display the output
//    private TextView txtScreen;
//    // Represent whether the lastly pressed key is numeric or not
//    private boolean lastNumeric;
//    // Represent that current state is in error or not
//    private boolean stateError;
//    // If true, do not allow to add another DOT
//    private boolean lastDot;

    private CalculatorData calc_data;

    //static final private int SIMPLE_CALC = 0;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (savedInstanceState != null){
//            calc_data = savedInstanceState.getParcelable("calc_data");
//        }
//        else
        calc_data=new CalculatorData("",false,false,false);
        //((TextShowingActivity)getActivity()).show(calc_data.getExpression());
    //    setRetainInstance(true);
        //savedInstanceState.putParcelable("calc_data", calc_data);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //super.onCreateView(inflater, container,savedInstanceState);///?????????
        View rootView = inflater.inflate(R.layout.simple_calculator, container, false);
//        SimpleCalculatorBinding binding = DataBindingUtil.bind(rootView);
        //this.txtScreen = (TextView) rootView.findViewById(R.id.textViewAns);

        setNumericOnClickListener(rootView);
        setOperatorOnClickListener(rootView);

        //binding.setFragment(this);
        ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
        return rootView;
    }



    public void change(CalculatorData data){
        data.setExpression(calc_data.getExpression()+ data.getExpression());
        calc_data.setExpression(data.getExpression());
        calc_data.setStateError(calc_data.getStateError());
        if (data.getLastIsNumber()){
            calc_data.setLastIsNumber(true);
        }
        ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.usual_calc_menu, menu);
//        return true;
//    }

    
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        Intent newInt = new Intent(this, ScientificCalc.class);
//        newInt.putExtra("calc_data", calc_data);
//
//        startActivityForResult(newInt, SIMPLE_CALC);
//        return true;
//    }


    //Save data when rotate
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("calc_data", calc_data);
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            calc_data = savedInstanceState.getParcelable("calc_data");
            ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
        }


    }

    private void append_to_expression(String str){
        String expression = calc_data.getExpression().concat(str);
        calc_data.setExpression(expression);
    }

    private void setNumericOnClickListener(View view) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button = (Button) v;
                if (calc_data.getStateError()) {
                    calc_data.setLastIsNumber(true);
                    calc_data.setExpression(button.getText().toString());
                    calc_data.setStateError(false);
                } else {
                    calc_data.setLastIsNumber(true);
                    append_to_expression(button.getText().toString());
                }
                ((TextShowingActivity)getActivity()).show(calc_data.getExpression());

            }
        };
        // Assign the listener to all the numeric buttons
        for (int id : numericButtons) {
            view.findViewById(id).setOnClickListener(listener);
        }
    }

    private void setOperatorOnClickListener(View view) {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calc_data.getLastIsNumber() && !calc_data.getStateError()) {
                    Button button = (Button) v;

                    append_to_expression(button.getText().toString());

                    ((TextShowingActivity)getActivity()).show(calc_data.getExpression());

                    calc_data.setLastIsNumber(false);
                    calc_data.setIsDotLast(false);    // Reset the DOT flag
                }
            }
        };
        for (int id : operatorButtons) {
            view.findViewById(id).setOnClickListener(listener);
        }
        view.findViewById(R.id.btnDot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calc_data.getLastIsNumber()&& !calc_data.getStateError() && !calc_data.getIsDotLast()) {

                    append_to_expression(".");
                    ((TextShowingActivity)getActivity()).show(calc_data.getExpression());

                    calc_data.setLastIsNumber(false);
                    calc_data.setIsDotLast(true);
                }
            }
        });
        /*view.findViewById(R.id.btnDel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtScreen.setText("");
                calc_data.setLastIsNumber(false);
                calc_data.setStateError(false);
                calc_data.setIsDotLast(false);
            }
        });*/
        // Equal button
        view.findViewById(R.id.btnEqual).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEqual();
            }
        });
    }

    public void reset_calc_data(){
        if (!(calc_data == null)){
            calc_data.setExpression("");
            calc_data.setLastIsNumber(false);
            calc_data.setStateError(false);
            calc_data.setIsDotLast(false);
            ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
        }


    }

    @Override
    public  void setUserVisibleHint(boolean isVisibleToUser){
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && calc_data!=null)
            ((TextShowingActivity)getActivity()).show(calc_data.getExpression());
    }

    private void onEqual() {
        try {
            if (calc_data.getLastIsNumber() && !calc_data.getStateError()) {
                calc_data.setExpression(calc_data.getExpression().replaceAll("e", String.valueOf(Math.E)).replaceAll("pi", String.valueOf(Math.PI)).replaceAll("^", "exp"));

                Expression expression = new ExpressionBuilder(calc_data.getExpression()).build();
                double result = expression.evaluate();
                calc_data.setExpression(Double.toString(result));
                calc_data.setIsDotLast(true); // Result contains a dot
            }
            } catch (IllegalArgumentException|ArithmeticException|EmptyStackException e) {
                calc_data.setExpression(getResources().getString(R.string.error));
                calc_data.setStateError(true);
                calc_data.setLastIsNumber(false);
            } finally {
                ((TextShowingActivity) getActivity()).show(calc_data.getExpression());
            }
        }
    }






    /*
    public static final String ADD = "\u002B";
    public static final String SUB = "\u2212";
    public static final String DIV = "\u00F7";
    public static final String MUL = "\u2715";


    private String value = "";
    public String getValue(){
        return value;
    }
    public int maxLength = 20;
    public LinkedList<String> operators = new LinkedList<String>();

    //TextView tvAns;

    public SimpleCalculator() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.simple_calculator, container, false);
      //  tvAns = (TextView) rootView.findViewById(com.kornilo.admin.myapplication.R.id.textViewAns);
        SimpleCalculatorBinding binding = DataBindingUtil.bind(rootView);
        binding.setFragment(this);
        return rootView;
    }

    @Override
    public void onStart() {
        value = ((MainActivity) getActivity()).getValue();
        operators = ((MainActivity) getActivity()).operators;
        super.onStart();
    }

    @Override
    public void onPause() {
        ((MainActivity) getActivity()).setValue(value);
        ((MainActivity) getActivity()).operators = operators;
        super.onPause();
    }


    public void registerKey(View view)
    {
        int viewId = view.getId();
        if (!(getValue().length() == maxLength && viewId!=com.kornilo.admin.myapplication.R.id.buttonDel))
            switch(viewId)
            {
                case com.kornilo.admin.myapplication.R.id.button0:
                    safelyPlaceOperand("0");
                    break;
                case com.kornilo.admin.myapplication.R.id.button1:
                    safelyPlaceOperand("1");
                    break;
                case com.kornilo.admin.myapplication.R.id.button2:
                    safelyPlaceOperand("2");
                    break;
                case com.kornilo.admin.myapplication.R.id.button3:
                    safelyPlaceOperand("3");
                    break;
                case com.kornilo.admin.myapplication.R.id.button4:
                    safelyPlaceOperand("4");
                    break;
                case com.kornilo.admin.myapplication.R.id.button5:
                    safelyPlaceOperand("5");
                    break;
                case com.kornilo.admin.myapplication.R.id.button6:
                    safelyPlaceOperand("6");
                    break;
                case com.kornilo.admin.myapplication.R.id.button7:
                    safelyPlaceOperand("7");
                    break;
                case com.kornilo.admin.myapplication.R.id.button8:
                    safelyPlaceOperand("8");
                    break;
                case com.kornilo.admin.myapplication.R.id.button9:
                    safelyPlaceOperand("9");
                    break;
                case com.kornilo.admin.myapplication.R.id.buttonAdd:
                    safelyPlaceOperator(ADD);
                    break;
                case com.kornilo.admin.myapplication.R.id.buttonSub:
                    safelyPlaceOperator(SUB);
                    break;
                case com.kornilo.admin.myapplication.R.id.buttonDiv:
                    safelyPlaceOperator(DIV);
                    break;
                case com.kornilo.admin.myapplication.R.id.buttonMul:
                    safelyPlaceOperator(MUL);
                    break;
//                case com.kornilo.admin.myapplication.R.id.buttonDel:
//                    deleteFromLeft();
//                    break;
            }
        ((MainActivity) getActivity()).display(value);
    }

    private void display()
    {
        //TextView tvAns = (TextView) findViewById(com.kornilo.admin.myapplication.R.id.textViewAns);
        tvAns.setText(value);
    }

    //private void display(String s)
  //  {
//        TextView tvAns = (TextView) findViewById(com.kornilo.admin.myapplication.R.id.textViewAns);
//        tvAns.setText(s);
    //}

    private void safelyPlaceOperand(String op)
    {
        int operator_idx = findLastOperator();
        boolean last_symbol_is_not_operator = operator_idx != value.length()-1;
        // Avoid double zeroes in cases where it's illegal (e.g. 00 -> 0, 01 -> 1, 1+01 -> 1+1).
        //if (last_symbol_is_digit){
        if (last_symbol_is_not_operator) {
            boolean symbol_after_last_operator_is_zero = value.charAt(operator_idx + 1) == '0';
            if (symbol_after_last_operator_is_zero)
                deleteTrailingZero();
        }
        value += op;
    }

    private void safelyPlaceOperator(String op)
    {
        if (endsWithOperator())  // Avoid double operators by replacing old operator.
        {
            ((MainActivity) getActivity()).deleteFromLeft();
            value += op;
            operators.add(op);
        }
        else if (endsWithNumber())  // Safe to place operator right away.
        {
            value += op;
            operators.add(op);
        }
        // else: Operator by itself is an illegal operation, do not place operator.
    }

    private void deleteTrailingZero()
    {
        if (value.endsWith("0")) ((MainActivity) getActivity()).deleteFromLeft();
    }

//    public void deleteFromLeft()
//    {
//        if (value.length() > 0)
//        {
//            if (endsWithOperator()) operators.removeLast();
//            value = value.substring(0, value.length()-1);
//        }
//    }

    private boolean endsWithNumber()
    {
        // If we had a decimal point button, we'd have to grab the digit before it.
        return value.length() > 0 && Character.isDigit(value.charAt(value.length()-1));
    }

    private boolean endsWithOperator()
    {
        return (value.endsWith(ADD) || value.endsWith(SUB) || value.endsWith(MUL) || value.endsWith(DIV));
    }

    private int findLastOperator()
    {
        int add_idx = value.lastIndexOf(ADD);
        int sub_idx = value.lastIndexOf(SUB);
        int mul_idx = value.lastIndexOf(MUL);
        int div_idx = value.lastIndexOf(DIV);
        return Math.max(add_idx, Math.max(sub_idx, Math.max(mul_idx, div_idx)));
    }
////
////    public void calculate(View view)  // Note: only called by the '=' button.
////    {
////        if (operators.isEmpty()) return;  // There is no operation to perform yet.
////        if (endsWithOperator())
////        {
////            ((MainActivity) getActivity()).display("incorrect format");
////            resetCalculator();
////            return;
////        }
////
////        // StringTokenizer is deprecated. Using String.split instead.
////        String[] operands = value.split("\\u002B|\\u2212|\\u00F7|\\u2715");
////
////        int i = 0;
////        double ans = Double.parseDouble(operands[i]); // TODO: catch NumberFormatException?
////        for (String operator : operators)
////            ans = applyOperation(operator, ans, Double.parseDouble(operands[++i]));
////
////        DecimalFormat df = new DecimalFormat("0.###");
////        ((MainActivity) getActivity()).display(df.format(ans));
////        resetCalculator();
////        // TODO: overwrite value with ans. reset value on next keypress if it's an operand (leave it if it's an operator)
////    }
//
//    private double applyOperation(String operator, double operand1, double operand2)
//    {
//        // Not using the string with switch syntax because Android doesn't support JDK 7 (JRE 1.7) yet.
//        if (operator.equals(ADD)) return operand1 + operand2;
//        if (operator.equals(SUB)) return operand1 - operand2;
//        if (operator.equals(MUL)) return operand1 * operand2;
//        if (operator.equals(DIV)) return operand1 / operand2;  // Don't need to check for div by 0, Java already does.
//        return 0.0;
//    }
//
//    private void resetCalculator()
//    {
//        value = "";
//        operators.clear();
//    }

    //public SendToActivity
    */
//}
